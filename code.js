import React, { useState } from "react"

let comments = [
  {
    id: 1,
    text: "message 1"
  },
  {
    id: 2,
    text: "message 2",
    children: [
      {
        id: 4,
        text: "message 4",
        children: [
          {
            id: 7,
            text: "message 7"
          },
          {
            id: 8,
            text: "message 8"
          }
        ]
      },
      {
        id: 5,
        text: "message 5"
      }
    ]
  }
];

const Comments = (props) => {
  const { comments, ...rest } = props;

  return (
    <ul>
      {comments.map(({ id, text, children }, index) => (
        <CommentsData index={index} id={id} text={text} children={children} />
      ))}
    </ul>
  );
};

const CommentsData = (props) => {
  const { index, id, text, children, ...rest } = props;

  const [visible, setVisible] = useState(true);
  const toogleVisible = () => setVisible((prevState) => !prevState);

  return (
    <li key={index}>
      <div onClick={toogleVisible}>
        {children && <h3>Click for {!visible ? "open" : "hide"} block</h3>}
        {
          <span>
            Id: {id}; Test: {text}
          </span>
        }
      </div>

      {visible && children && <Comments comments={children} />}
    </li>
  )
}

const App = () => {
  return (
    <div className="App">
      <div>
        <Comments comments={comments} />
      </div>
    </div>
  )
}

export default App
